#! env php
<?php

use LGnap\Commands\DeleteImagesCommand;
use Symfony\Component\Console\Application;

require_once 'vendor/autoload.php';

$app = new Application();

$app->add(new DeleteImagesCommand());

$app->run();

/* It's not enough.
You have to restart your registry with this custom command:
> garbage-collect /etc/docker/registry/config.yml
After you can remove this custom command and go back to usual process


And if you want to remove some non used repositories (non used/test entries)
Go into the mounted volume (on FS directly, registry shutted off) into `docker/registry/v2/repositories`
and `rm -rf` manually repository you want to delete
*/
