<?php
namespace LGnap\Commands;


use LGnap\HttpClient\RegistryClient;
use RuntimeException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class DeleteImagesCommand extends Command
{

    const GO_BACK = 'go back';
    protected static $defaultName = 'delete';
    /** @var RegistryClient */
    private $registryClient;


    protected function configure()
    {
        $this
            ->setDescription('Delete images/tags from docker registry.')
            ->setHelp('This command allows you to browse a registry and clean it easy...')
        ;

        $this
            ->addArgument('server_url', InputArgument::REQUIRED, 'Registry url')
            ->addArgument('user', InputArgument::OPTIONAL, 'Registry\' user login')
            ->addArgument('password', InputArgument::OPTIONAL, 'Registry\' user password')
        ;
    }

    private function chooseItemFromList(InputInterface $input, OutputInterface $output, array $choices, string $thing){
        $question = new ChoiceQuestion(
            'Please select the ' . $thing . ' to inspect',
            array_merge($choices, [self::GO_BACK]),
            count($choices) // array size (will select goback as default)
        );

        return $this->getHelper('question')->ask($input, $output, $question);
    }

    private function getValidationFromUser(InputInterface $input, OutputInterface $output) {
        $question = new ConfirmationQuestion("Delete this?\n", false);

        return $this->getHelper('question')->ask($input, $output, $question);
    }

    private function chooseRepository(InputInterface $input, OutputInterface $output, array $repositories) {
        return $this->chooseItemFromList($input, $output, $repositories, 'repo');
    }

    private function chooseTag(InputInterface $input, OutputInterface $output, array $tags) {
        return $this->chooseItemFromList($input, $output, $tags, 'tag');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        while(true) {
            $repositories = $this->registryClient->extractRepositories();

            $repositoriesWithTags = [];
            foreach ($repositories as $repository) {
                $repositoriesWithTags[] =
                $repository
                . ' - '
                . count($this->registryClient->listTags($repository));

            }

            $repositorySelected = $this->chooseRepository($input, $output, $repositoriesWithTags);
            if ($repositorySelected === self::GO_BACK) {
                break;
            }

            //remove tags counter
            $pos = strpos($repositorySelected, ' - ');
            $repositorySelected = substr($repositorySelected, 0, $pos);

            while (true) {
                $tags = $this->registryClient->listTags($repositorySelected);
                if(! $tags) {
                    $output->writeln('No tags found for this repo "' . $repositorySelected . '"');
                    break;
                }

                $tagSelected = $this->chooseTag($input, $output, $tags);
                if ($tagSelected === self::GO_BACK) {
                    break;
                }

                $digest = $this->registryClient->extractDigest($repositorySelected, $tagSelected);

                $isValidDigest = $this->registryClient->validateDigest($repositorySelected, $digest);

                if (!$isValidDigest) {
                    throw new RuntimeException('Invalid digest found');
                }

                $okToDelete = $this->getValidationFromUser($input, $output);

                if($okToDelete) {
                    $this->registryClient->deleteDigest($repositorySelected, $digest);
                }
            }
        }
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $this->registryClient->testLink();
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->registryClient = new RegistryClient(
            $input->getArgument('server_url'),
            $input->getArgument('user'),
            $input->getArgument('password')
        );
    }
}
