<?php
namespace LGnap\HttpClient;


use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\ResponseInterface;
use UnexpectedValueException;

class RegistryClient
{
    /** @var \Symfony\Contracts\HttpClient\HttpClientInterface */
    private $httpClient;

    /** @var string */
    private $registryUrl;

    /**
     * RegistryClient constructor.
     * @param string $registryUrl
     * @param string|null $user
     * @param string|null $password
     */
    public function __construct(string $registryUrl, ?string $user, ?string $password)
    {
        $this->registryUrl = $registryUrl;

        $httpClientOptions = [
            'http_version' => '2.0',
            'headers' => [
                'Accept' => 'application/vnd.docker.distribution.manifest.v2+json'
            ]
        ];

        if ($user && $password) {
            $httpClientOptions['auth_basic'] = [$user, $password];
        }

        $this->httpClient = HttpClient::create($httpClientOptions);
    }

    /**
     * @param ResponseInterface $response
     * @param array $allowedStatusCodes
     * @return array
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    private function handleRequest(ResponseInterface $response, array $allowedStatusCodes = [200]) {

        if (! in_array($response->getStatusCode() , $allowedStatusCodes)) {
            throw new UnexpectedValueException('Status code != 200: ' . var_export($response->getInfo(), true), $response->getStatusCode());
        }
        if ($response->getInfo()['http_method'] === 'HEAD') {
            return $response->getHeaders();
        }
        return $response->toArray();
    }

    /**
     * @param string $apiCall
     * @param string $method
     * @return ResponseInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    private function makeBasicRequest(string $apiCall, string $method = 'GET') {
        return $this->httpClient->request($method, $this->buildRequest($apiCall));
    }

    private function buildRequest($apiCall) {
        return $this->registryUrl .  '/v2/' . $apiCall;
    }

    /**
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function testLink() {
        // TODO if 401 => auth error
        $this->handleRequest($this->makeBasicRequest(''));
    }

    /**
     * @return string[]
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function extractRepositories() {
        $response = $this->handleRequest($this->makeBasicRequest('_catalog'));
        if (! array_key_exists('repositories', $response)) {
            throw new UnexpectedValueException('repositories key not found');
        }
        return $response['repositories'];
    }

    /**
     * @param string $repository
     * @return string[]
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function listTags(string $repository)
    {
        $response = $this->handleRequest( $this->makeBasicRequest($repository .  '/tags/list'));

        if (! array_key_exists('tags', $response)) {
            throw new UnexpectedValueException('tags key not found');
        }

        return $response['tags'] ?? [];
    }

    /**
     * @param string $repository
     * @param string $fromTag
     * @return mixed
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function extractDigest(string $repository, string $fromTag) {
        $headers = $this->handleRequest( $this->makeBasicRequest($repository .  '/manifests/' . $fromTag, 'HEAD'));

        if (! array_key_exists('docker-content-digest', $headers)) {
            throw new UnexpectedValueException('docker-content-digest key not found');
        }

        //0 will exist everytime, empty but exists if header is set
        return $headers['docker-content-digest'][0];
    }

    /**
     * @param string $repository
     * @param string $tagDigest
     * @return bool
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function validateDigest(string $repository, string $tagDigest) {
        // 500 => invalid sha256
        // 404 => digest not found
        try{
            $this->handleRequest( $this->makeBasicRequest($repository .  '/manifests/' . $tagDigest, 'HEAD'));
            return true;
        }catch (UnexpectedValueException $unexpectedValue){
            return false;
        }
    }

    /**
     * @param string $repository
     * @param string $tagDigest
     * @return bool
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function deleteDigest(string $repository, string $tagDigest) {
        try{
            $this->handleRequest( $this->makeBasicRequest($repository .  '/manifests/' . $tagDigest, 'DELETE'), [200, 204]);
            return true;
        }catch (UnexpectedValueException $unexpectedValue){
            return false;
        }
    }
}
